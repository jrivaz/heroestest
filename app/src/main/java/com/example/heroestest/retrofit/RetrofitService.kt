package com.example.heroestest.retrofit

import com.example.heroestest.models.HeroResponse
import com.example.heroestest.models.HeroesResponse
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import okhttp3.OkHttpClient


import okhttp3.logging.HttpLoggingInterceptor


interface RetrofitService {

    @GET("search/{name}")
    suspend fun findByName(@Path("name") id: String): Response<HeroesResponse>

    @GET("{id}")
    suspend fun findById(@Path("id") id: String): Response<HeroResponse>

    @GET("{id}/image")
    suspend fun findImageAndNameById(@Path("id") id: String): Response<HeroResponse>


    companion object {
        var retrofitService: RetrofitService? = null
        fun getInstance(): RetrofitService {
            if (retrofitService == null) {
                val logging = HttpLoggingInterceptor()
                logging.setLevel(HttpLoggingInterceptor.Level.BASIC)
                val client: OkHttpClient = OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .build()
                val retrofit = Retrofit.Builder()
                    .baseUrl("https://superheroapi.com/api/6522263554515260/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build()
                retrofitService = retrofit.create(RetrofitService::class.java)
            }
            return retrofitService!!
        }

    }
}