package com.example.heroestest.repository

import com.example.heroestest.retrofit.RetrofitService

class HeroesRepository constructor(private val retrofitService: RetrofitService) {

    suspend fun findByName(name: String) = retrofitService.findByName(name)
    suspend fun findById(id: String) = retrofitService.findById(id)
    suspend fun findImageAndNameById(id: String) = retrofitService.findImageAndNameById(id)

}