package com.example.heroestest.viewModelFactory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.heroestest.repository.HeroesRepository
import com.example.heroestest.ui.heroDetail.HeroDetailViewModel
import com.example.heroestest.ui.heroesList.HeroesListViewModel

class ViewModelFactory constructor(private val repository: HeroesRepository): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(HeroesListViewModel::class.java) -> {
                HeroesListViewModel(this.repository) as T
            }
            modelClass.isAssignableFrom(HeroDetailViewModel::class.java) -> {
                HeroDetailViewModel(this.repository) as T
            }
            else -> {
                throw IllegalArgumentException("ViewModel Not Found")
            }
        }
    }
}