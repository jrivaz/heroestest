package com.example.heroestest.ui.heroesList.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.heroestest.R
import com.example.heroestest.databinding.HeroRvViewBinding
import com.example.heroestest.models.HeroResponse
import com.example.heroestest.ui.heroesList.HeroesListViewModel

class HeroAdapter(
    private val viewModel: HeroesListViewModel,
    private val context: Context,
    private val clickHero: ClickHero
) :
    RecyclerView.Adapter<MainViewHolder>() {

    var heroesList = mutableListOf<HeroResponse>()

    fun setHeroes(heroess: List<HeroResponse>) {
        this.heroesList = heroess.toMutableList()
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = HeroRvViewBinding.inflate(inflater, parent, false)
        return MainViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val hero = heroesList[position]
        val circularProgressDrawable = initCircularProgress()
        if (hero.name == "loading") {
            Glide.with(holder.itemView.context)
                .load(circularProgressDrawable).into(holder.binding.imgHero)
        } else {
            holder.binding.tvName.text = hero.name
            Glide.with(holder.itemView.context).applyDefaultRequestOptions(
                RequestOptions().placeholder(
                    circularProgressDrawable
                ).error(
                    R.drawable.noimageplaceholder
                )
            ).load(hero.url).into(holder.binding.imgHero)
            if (heroesList.size - 1 == position + 1)
                viewModel.nextHero()
        }
        setupClickListener(holder, hero)

    }

    private fun setupClickListener(holder: MainViewHolder, hero: HeroResponse) {
        holder.binding.container.setOnClickListener {
            clickHero.clickHero(hero.id, hero.name)
        }
    }

    private fun initCircularProgress(): CircularProgressDrawable {
        val circularProgressDrawable = CircularProgressDrawable(context)
        circularProgressDrawable.strokeWidth = 10f
        circularProgressDrawable.centerRadius = 50f
        circularProgressDrawable.start()
        return circularProgressDrawable
    }

    override fun getItemCount(): Int {
        return heroesList.size
    }
}

interface ClickHero {
    fun clickHero(heroId: String, heroName: String)
}

class MainViewHolder(val binding: HeroRvViewBinding) : RecyclerView.ViewHolder(binding.root) {

}