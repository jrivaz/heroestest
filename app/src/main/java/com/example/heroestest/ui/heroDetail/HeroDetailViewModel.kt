package com.example.heroestest.ui.heroDetail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.heroestest.models.HeroResponse
import com.example.heroestest.repository.HeroesRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response

class HeroDetailViewModel constructor(private val repository: HeroesRepository)  : ViewModel() {
    val hero = MutableLiveData<HeroResponse>()
    val loading = MutableLiveData<Boolean>()
    val error = MutableLiveData<String>()

    fun getHeroById(id: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.findById(id)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    validateResponse(response)
                    loading.value = false
                } else {
                    error.value = response.message()
                }
            }
        }

    }

    private fun validateResponse(response: Response<HeroResponse>) {
        if (response.body()!!.response == "success")
            processSuccess(response)
        else
            processError(response)

    }

    private fun processError(response: Response<HeroResponse>) {
        error.value = response.body()!!.error
    }

    private fun processSuccess(response: Response<HeroResponse>) {
        hero.postValue(response.body())
    }
}