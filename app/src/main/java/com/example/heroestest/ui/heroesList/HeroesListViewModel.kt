package com.example.heroestest.ui.heroesList

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.heroestest.models.HeroResponse
import com.example.heroestest.repository.HeroesRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response

class HeroesListViewModel constructor(private val repository: HeroesRepository) : ViewModel() {

    private val listHeroesAux = ArrayList<HeroResponse>()
    private var heroId = 0
    private var canLoad = true
    val listHeroes = MutableLiveData<List<HeroResponse>>()
    val loading = MutableLiveData<Boolean>()
    val error = MutableLiveData<String>()

    private fun getHeroNameAndImage(id: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.findImageAndNameById(id)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    validateResponse(response)
                    loading.value = false
                } else {
                    error.value = response.message()
                }
            }
        }

    }

    private fun validateResponse(response: Response<HeroResponse>) {
        if (response.body()!!.response == "success")
            processSuccess(response)
        else
            processError(response)

    }

    private fun processError(response: Response<HeroResponse>) {
        listHeroesAux.removeLast()
        listHeroes.postValue(listHeroesAux)
        error.value = response.body()!!.error
        canLoad = false
    }

    private fun processSuccess(response: Response<HeroResponse>) {
        if (listHeroesAux.isNotEmpty())
            listHeroesAux.removeLast()
        listHeroesAux.add(response.body()!!)
        listHeroesAux.add(HeroResponse(name = "loading"))
        listHeroes.postValue(listHeroesAux)
    }

    fun nextHero() {
        if (canLoad) {
            heroId++
            getHeroNameAndImage(heroId.toString())
        }
    }
}