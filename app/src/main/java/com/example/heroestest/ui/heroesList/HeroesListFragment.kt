package com.example.heroestest.ui.heroesList

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.example.heroestest.R
import com.example.heroestest.databinding.FragmentHeroesListBinding
import com.example.heroestest.repository.HeroesRepository
import com.example.heroestest.retrofit.RetrofitService
import com.example.heroestest.ui.heroesList.adapter.ClickHero
import com.example.heroestest.ui.heroesList.adapter.HeroAdapter
import com.example.heroestest.viewModelFactory.ViewModelFactory


class HeroesListFragment : Fragment(), ClickHero {

    private lateinit var binding: FragmentHeroesListBinding
    private lateinit var viewModel: HeroesListViewModel
    private lateinit var adapter: HeroAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHeroesListBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        setTitle()
        setHomeButton()
        initViewModel()
        setupList()
        listHeroesObserver()
        errorObserver()
        loadingObserver()
        firstHero()
    }

    private fun setHomeButton() {
        (activity as? AppCompatActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(false)
    }

    private fun setTitle() {
        requireActivity().setTitle(R.string.app_name)
    }

    private fun initViewModel() {
        val retrofitService = RetrofitService.getInstance()
        val heroesRepository = HeroesRepository(retrofitService)
        viewModel = ViewModelProvider(
            this,
            ViewModelFactory(heroesRepository)
        )[HeroesListViewModel::class.java]
    }

    private fun setupList() {
        adapter = HeroAdapter(viewModel, requireContext(), this)
        binding.rvHeroes.adapter = adapter
    }

    private fun listHeroesObserver() {
        viewModel.listHeroes.observe(viewLifecycleOwner, {
            adapter.setHeroes(it)
        })
    }

    private fun errorObserver() {
        viewModel.error.observe(viewLifecycleOwner, {
            if (it.isNotEmpty())
                Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
        })
    }

    private fun loadingObserver() {
        viewModel.loading.observe(viewLifecycleOwner, {
            if (it) {
                binding.progressDialog.visibility = View.VISIBLE
            } else {
                binding.progressDialog.visibility = View.GONE
            }
        })
    }

    private fun firstHero() {
        viewModel.nextHero()
    }

    override fun clickHero(heroId: String, heroName: String) {
        val bundle = Bundle()
        bundle.putString("heroId", heroId)
        bundle.putString("heroName", heroName)
        requireActivity().findNavController(R.id.nav_host).navigate(R.id.list_to_detail, bundle)
    }


}