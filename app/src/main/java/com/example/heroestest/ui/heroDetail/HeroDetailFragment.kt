package com.example.heroestest.ui.heroDetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.heroestest.R
import com.example.heroestest.databinding.FragmentHeroDetailBinding
import com.example.heroestest.models.HeroResponse
import com.example.heroestest.models.heroSubModels.*
import com.example.heroestest.repository.HeroesRepository
import com.example.heroestest.retrofit.RetrofitService
import com.example.heroestest.viewModelFactory.ViewModelFactory

class HeroDetailFragment : Fragment() {


    private lateinit var viewModel: HeroDetailViewModel
    private lateinit var binding: FragmentHeroDetailBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHeroDetailBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        setTitle()
        setHomeButton()
        initViewModel()
        errorObserver()
        loadingObserver()
        heroObserver()
        loadHero()
    }

    private fun setHomeButton() {
        (activity as? AppCompatActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun setTitle() {
        requireActivity().title = requireArguments().getString("heroName", "")
    }

    private fun loadHero() {
        viewModel.getHeroById(requireArguments().getString("heroId", "0"))
    }

    private fun errorObserver() {
        viewModel.error.observe(viewLifecycleOwner, {
            if (it.isNotEmpty())
                Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
            requireActivity().onBackPressed()
        })
    }

    private fun loadingObserver() {
        viewModel.loading.observe(viewLifecycleOwner, {
            if (it) {
                binding.progressDialog.visibility = View.VISIBLE
            } else {
                binding.progressDialog.visibility = View.GONE
            }
        })
    }

    private fun initViewModel() {
        val retrofitService = RetrofitService.getInstance()
        val heroesRepository = HeroesRepository(retrofitService)
        viewModel = ViewModelProvider(
            this,
            ViewModelFactory(heroesRepository)
        )[HeroDetailViewModel::class.java]
    }

    private fun heroObserver() {
        viewModel.hero.observe(viewLifecycleOwner, {
            fillData(it!!)
        })
    }

    private fun fillData(it: HeroResponse) {
        fillImage(it.image)
        fillPowerStats(it.powerStats)
        fillBiography(it.biography)
        fillAppearance(it.appearance)
        fillWork(it.work)
        fillConnections(it.connections)
    }

    private fun fillWork(work: HeroWork?) {
        binding.tvOccupation.text = work!!.occupation
        binding.tvBase.text = work.base
    }

    private fun fillConnections(connections: HeroConnections?) {
        binding.tvGroupAffiliation.text = connections!!.groupAffiliation
        binding.tvRelatives.text = connections.relatives
    }

    private fun fillAppearance(appearance: HeroAppearance?) {
        binding.tvGender.text = appearance!!.gender
        binding.tvRace.text = appearance.race
        binding.tvHeight.text = appearance.getHeight()
        binding.tvWeight.text = appearance.getWeight()
        binding.tvEyesColor.text = appearance.eyeColor
        binding.tvHairColor.text = appearance.hairColor
    }

    private fun fillBiography(biography: HeroBiography?) {
        binding.tvFullName.text = biography!!.fullName
        binding.tvAlterEgos.text = biography.alterEgos
        binding.tvAliases.text = biography.getAliasString()
        binding.tvPlaceOfBirth.text = biography.placeOfBirth
        binding.tvFirstAppearance.text = biography.firstAppearance
        binding.tvPublisher.text = biography.publisher
        binding.tvAlignment.text = biography.alignment
    }

    private fun fillPowerStats(powerStats: HeroPowerStats?) {
        binding.tvIntelligence.text =
            if (powerStats!!.intelligence == "null") "-" else powerStats.intelligence
        binding.progressIntelligence.progress =
            if (powerStats.intelligence == "null") 0 else Integer.parseInt(powerStats.intelligence)
        binding.tvStrength.text = if (powerStats.strength == "null") "-" else powerStats.strength
        binding.progressStrength.progress =
            if (powerStats.strength == "null") 0 else Integer.parseInt(powerStats.strength)
        binding.tvSpeed.text = if (powerStats.speed == "null") "-" else powerStats.speed
        binding.progressSpeed.progress =
            if (powerStats.speed == "null") 0 else Integer.parseInt(powerStats.speed)
        binding.tvDurability.text =
            if (powerStats.durability == "null") "-" else powerStats.durability
        binding.progressDurability.progress =
            if (powerStats.durability == "null") 0 else Integer.parseInt(powerStats.durability)
        binding.tvPower.text = if (powerStats.power == "null") "-" else powerStats.power
        binding.progressPower.progress =
            if (powerStats.power == "null") 0 else Integer.parseInt(powerStats.power)
        binding.tvCombat.text = if (powerStats.combat == "null") "-" else powerStats.combat
        binding.progressCombat.progress =
            if (powerStats.combat == "null") 0 else Integer.parseInt(powerStats.combat)
    }

    private fun fillImage(image: HeroImage?) {
        val circularProgressDrawable = initCircularProgress()
        Glide.with(requireContext()).applyDefaultRequestOptions(
            RequestOptions().placeholder(
                circularProgressDrawable
            ).error(
                R.drawable.noimageplaceholder
            )
        ).load(image!!.imageUrl).into(binding.imgHero)
    }

    private fun initCircularProgress(): CircularProgressDrawable {
        val circularProgressDrawable = CircularProgressDrawable(requireContext())
        circularProgressDrawable.strokeWidth = 10f
        circularProgressDrawable.centerRadius = 50f
        circularProgressDrawable.start()
        return circularProgressDrawable
    }
}