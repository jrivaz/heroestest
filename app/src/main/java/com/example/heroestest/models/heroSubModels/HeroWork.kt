package com.example.heroestest.models.heroSubModels

data class HeroWork(
    val occupation: String,
    val base: String
)