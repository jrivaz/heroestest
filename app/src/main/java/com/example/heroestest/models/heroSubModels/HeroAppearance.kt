package com.example.heroestest.models.heroSubModels

import com.google.gson.annotations.SerializedName

data class HeroAppearance(
    val gender: String,
    val race: String,
    val height: List<String>,
    val weight: List<String>,
    @SerializedName("eye-color")
    val eyeColor : String,
    @SerializedName("hair-color")
    val hairColor: String

) {
    fun getHeight(): String {
        var heightString = ""
        for(aux in height){
            heightString += if(heightString.isEmpty())
                aux
            else
                " / $aux"
        }
        return heightString
    }

    fun getWeight(): String {
        var weightString = ""
        for(aux in weight){
            weightString += if(weightString.isEmpty())
                aux
            else
                " / $aux"
        }
        return weightString
    }
}