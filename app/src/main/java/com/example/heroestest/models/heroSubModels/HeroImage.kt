package com.example.heroestest.models.heroSubModels

import com.google.gson.annotations.SerializedName

data class HeroImage(
    @SerializedName("url") val imageUrl: String
)