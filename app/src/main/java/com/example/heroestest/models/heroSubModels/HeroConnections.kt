package com.example.heroestest.models.heroSubModels

import com.google.gson.annotations.SerializedName


data class HeroConnections(
    @SerializedName("group-affiliation")
    val groupAffiliation: String,
    val relatives: String
)