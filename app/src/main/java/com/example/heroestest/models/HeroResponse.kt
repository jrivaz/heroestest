package com.example.heroestest.models

import com.example.heroestest.models.heroSubModels.*
import com.google.gson.annotations.SerializedName

data class HeroResponse(
    val response: String="",
    val id: String="",
    val name: String="",
    val url: String="",
    @SerializedName("powerstats")
    val powerStats: HeroPowerStats?=null,
    val biography: HeroBiography?=null,
    val appearance: HeroAppearance?=null,
    val work: HeroWork?=null,
    val connections: HeroConnections?=null,
    val image: HeroImage?=null,
    val error: String=""

)