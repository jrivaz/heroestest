package com.example.heroestest.models.heroSubModels

import com.google.gson.annotations.SerializedName

data class HeroBiography(
    @SerializedName("full-name")
    val fullName: String,
    @SerializedName("alter-egos")
    val alterEgos: String,
    @SerializedName("aliases")
    val alias: List<String>,
    @SerializedName("place-of-birth")
    val placeOfBirth: String,
    @SerializedName("first-appearance")
    val firstAppearance: String,
    val publisher: String,
    val alignment: String
) {
    fun getAliasString(): String {
        var aliasString = ""
        for(aux in alias){
            aliasString += if(aliasString.isEmpty())
                aux
            else
                ", $aux"
        }
        return aliasString
    }
}