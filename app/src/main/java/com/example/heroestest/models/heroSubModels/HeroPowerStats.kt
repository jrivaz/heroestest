package com.example.heroestest.models.heroSubModels

data class HeroPowerStats(
    val intelligence: String,
    val strength: String,
    val speed: String,
    val durability: String,
    val power: String,
    val combat: String
)