package com.example.heroestest.models

import com.google.gson.annotations.SerializedName

data class HeroesResponse(
    val response: String,
    @SerializedName("results-for")
    val response_for: String,
    val results: List<HeroResponse>
)